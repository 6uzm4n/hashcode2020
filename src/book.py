class Book:
    def __init__(self, id, score):
        self.id = id
        self.score = score

    def __str__(self):
        return '({}, {})'.format(self.id, self.score)

    def __ge__(self, other):
        return self.score >= other.score

    def __le__(self, other):
        return self.score <= other.score