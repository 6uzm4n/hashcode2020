from src.library import Library


class Solution:
    def __init__(self, libraries, books, n_days):
        self.rem_days = n_days
        self.rem_libraries = libraries
        self.rem_books = books
        self.used_books = []
        self.solution = [] #[{'lib', 'used_books'}, ...]

    def write_output(self):
        text = '{}'.format(len(self.solution))
        for lib in self.solution:
            text += '\n{} {}'.format(int(lib['lib'].id), len(lib['used_books']))
            text += '\n{}'.format(' '.join([str(b.id) for b in lib['used_books']]))
        return text

    def find_solution(self):
        while True:
            choice = self.get_best_library()
            if choice is None:
                break
            self.solution.append({
                'lib': choice['lib'],
                'used_books': choice['books']
            })

            self.rem_days -= choice['lib'].register_days
            self.rem_libraries.remove(choice['lib'])
            for b in choice['books']:
                self.used_books.append(b)

            print('REM DAYS: ' + str(self.rem_days))

    def get_best_library(self):
        best_choice = {'lib': None, 'score': -1, 'books': None}
        i = 0
        for lib in self.rem_libraries:
            i += 1
            if lib.register_days >= self.rem_days:
                continue

            result = lib.calculate_book_score(self.used_books)
            if result['score'] > best_choice['score']:
                best_choice['lib'] = lib
                best_choice['score'] = result['score']
                best_choice['books'] = result['books']

        if best_choice['lib'] is None:
            return None
        else:
            return best_choice
