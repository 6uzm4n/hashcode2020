from src import input_reader
from src import output_writer
from src.solution import Solution


def main():
    for letter in ['a', 'b', 'c', 'd', 'e', 'f']:
        do_the_thing(letter)

def do_the_thing(letter):
    # read input
    input_value = input_reader.read_input_file(letter)

    sol = Solution(input_value['libraries'], input_value['books'], input_value['n_days'])
    sol.find_solution()
    output = sol.write_output()

    # write output
    output_file = '{}_output.txt'.format(letter)
    output_writer.write_output(output_file, output)


if __name__ == '__main__':
    main()
