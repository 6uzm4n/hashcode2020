from os import path


OUTPUT_REL_PATH = '../output'
OUTPUT_PATH = path.abspath(path.join(path.curdir, OUTPUT_REL_PATH))


def write_output(file_name, output_text):
    with open(path.abspath(path.join(OUTPUT_PATH, file_name)), 'w') as f:
        f.write(output_text)
