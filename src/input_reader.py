from os import listdir
from os import path
import re
from src.book import Book
from src.library import Library


INPUT_REL_PATH = '../input'
INPUT_PATH = path.abspath(path.join(path.curdir, INPUT_REL_PATH))


def read_input_file(letter):
    if letter not in ['a', 'b', 'c', 'd', 'e', 'f']:
        return None

    pattern = re.compile(r'{}_.*\.txt'.format(letter))

    files = [f for f in listdir(INPUT_PATH)
             if path.isfile(path.join(INPUT_PATH, f))
             and pattern.match(f)]

    if not files:
        return None

    input_file_path = path.abspath(path.join(INPUT_PATH, files[0]))

    with open(input_file_path) as input_file:
        return _parse_input_file(input_file)


def _parse_input_file(file):
    lines = file.readlines()

    # linea 0: n libros - n librerías - max días
    n_books, n_libr, n_days = lines[0].split(' ')
    print('read line 0')

    # línea 1: score libros
    books = [Book(id, int(score)) for id, score in enumerate(lines[1].split(' '))]
    print('read line 1')

    libraries = []
    index = 2
    while index < len(lines):
        line = lines[index]
        if line[0] == '\n':
            break
        line2 = lines[index + 1]
        # línea 1+n: n libros - dias registro - envio libros
        n_books, register_days, daily_books = line.split(' ')

        # línea 2+n: id libros
        libraries.append(Library((index/2)-1, [books[int(id)] for id in line2.split(' ')], int(register_days), int(daily_books)))

        index += 2
        print('read line {}'.format(index))
        print('read line {}'.format(index + 1))

    print ('done reading...')

    return {
        'n_days': int(n_days),
        'books': books,
        'libraries': libraries
    }
