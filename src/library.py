

class Library:
    def __init__(self, id, books, register_days, daily_books):
        self.id = id
        self.books = books
        self.register_days = register_days
        self.daily_books = daily_books

    def __str__(self):
        return '{}\n{}\n{}'.format(self.books, self.register_days, self.daily_books)

    def calculate_book_score(self, used_books):
        score = 0
        book_count = 0
        unused_books = []
        intersection = list(set(self.books) - set(used_books))
        for book in intersection:
            score += book.score
            unused_books.append(book)
            book_count += 1

        if len(intersection) > 0:
            score = pow(score, 2) / (len(intersection) / self.daily_books)
        else:
            score = 0

        days = book_count / self.daily_books
        if days < float(book_count) / self.daily_books:
            days += 1

        return {'score': score, 'days': days, 'books': unused_books}
